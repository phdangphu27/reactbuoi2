import React, { Component } from "react";
import GlassesList from "./GlassesList/GlassesList";
import { dataGlasses } from "../dataGlasses/dataGlasses";
import ModelWithGlasses from "./ModelWithGlasses/ModelWithGlasses";
import bg from "../assets/background.jpg";

export default class VirtualGlasses extends Component {
  state = {
    glasses: dataGlasses[0],
    glassesArr: dataGlasses,
  };

  handleChangeGlasses = (obj) => {
    this.setState({
      glasses: obj,
    });
  };

  render() {
    return (
      <div className='container'>
        <ModelWithGlasses glasses={this.state.glasses} />
        <GlassesList
          handleChangeGlasses={this.handleChangeGlasses}
          glassesData={this.state.glassesArr}
        />
      </div>
    );
  }
}
