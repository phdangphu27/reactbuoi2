import React, { Component } from "react";

export default class GlassesItem extends Component {
  render() {
    return (
      <div
        className='glasses p-3'
        onClick={() => {
          this.props.handleChangeGlasses(this.props.glasses);
        }}
      >
        <img
          className='h-100'
          style={{
            height: "80px",
            width: "230px",
          }}
          alt='true'
          src={this.props.glasses.url}
        />
      </div>
    );
  }
}
