import React, { Component } from "react";
import GlassesItem from "./GlassesItem/GlassesItem";

export default class GlassesList extends Component {
  renderGlasses = () => {
    return this.props.glassesData.map((glasses) => {
      return (
        <div className='row'>
          <GlassesItem
            handleChangeGlasses={this.props.handleChangeGlasses}
            glasses={glasses}
          />
        </div>
      );
    });
  };

  render() {
    return (
      <div>
        <div className='list'>{this.renderGlasses()}</div>
      </div>
    );
  }
}
