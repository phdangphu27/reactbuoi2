import "./App.css";
import VirtualGlasses from "./VirtualGlasses/VirtualGlasses";

function App() {
  return (
    <div className='App'>
      <VirtualGlasses />
    </div>
  );
}

export default App;
